import type { TurboModule } from 'react-native';
import { TurboModuleRegistry } from 'react-native';

export type Liveness =
  | 'live'
  | 'today'
  | 'this_week'
  | 'this_month'
  | 'habitual';

export type IABAudience = {
  id: string;
  extensions: string[];
};

export type Membership = {
  id: string;
  name: string;
  liveness: Liveness;
  extended_id: string;
  iab_ids: IABAudience[];
};

export interface Spec extends TurboModule {
  startRecording(apiKey: string): void;
  stopRecording(): void;
  currentMemberships(): Membership[];
}

export default TurboModuleRegistry.getEnforcing<Spec>('NeEuclid');
