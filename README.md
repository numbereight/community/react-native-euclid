# NumberEight Euclid for React Native

## Using this plugin

You will first need to [**generate a developer key**](https://portal.eu.numbereight.ai/keys) to authenticate with.

To install the plugin, run

```
$ yarn add https://gitlab.com/numbereight/community/react-native-ne-euclid
OR
$ npm install --save git+https://gitlab.com/numbereight/community/react-native-ne-euclid
```

## Plugin structure

This helper plugin is built using [create-react-native-library](https://github.com/callstack/react-native-builder-bob) to make a [Turbo Module](https://reactnative.dev/docs/next/the-new-architecture/pillars-turbomodules). This connects the NumberEight Android and iOS SDKs to a TypeScript wrapper.

The command used to bootstrap the project can be found in `./assemble_command.sh` should the project need to be regenerated.

The interface can be found in `src/`, the Android implementations in `android/` and the iOS implementation in `ios/`.

After changing the API in `src/`, it's necessary to re-run `yarn` to generate new interface definitions.

The example folder contains a sample project that can be built to test the API.

### Running the provided example project

If you don't yet have a React Native project, you can use the provided example as follows (you must have yarn installed):

1. Setup:
```
# Initialize
yarn

# Go to example project
cd example

# Put developer key in an environment variable
# (change <insert key> to your developer key)
echo 'NESDK_KEY=<insert key>' >> .env
```

2. Run Server:
```
yarn start
```

3. Run App:

**Android**: press 'a'

**iOS**: press 'i'

#### Platform-specific instructions

If required, refer to the official documentation:

* [Android](https://docs.numbereight.ai/#/android-insights)
* [iOS](https://docs.numbereight.ai/#/ios-insights)

#### Native repositories

You must make sure to add the NumberEight repository to your Android project:

##### Android (build.gradle)

```
allprojects {
    repositories {
        maven {
            url('https://repo.numbereight.ai/artifactory/gradle-release-local')
        }
    }
}
```

#### Permissions

While no permissions are required, for optimal performance and to avoid app store restrictions the recommended permission setups:

*  [AndroidManifest.xml for Android](https://docs.numbereight.ai/#/android-getting-started?id=example-androidmanifestxml)
*  [Info.plist for iOS](https://docs.numbereight.ai/documentation/#/ios-getting-started?id=declarations)

##### Location (optional)

In addition to adding the required settings to AndroidManifest.xml and Info.plist (see above links), some permissions such as location require a prompt to the user. If location is desired, NumberEight recommends using [react-native-permissions](https://github.com/react-native-community/react-native-permissions) for this task:

```
$ yarn add react-native-permissions
OR
$ npm install --save react-native-permissions
```

```
import { Platform } from 'react-native';
import { PERMISSIONS, request } from 'react-native-permissions';

request(
  Platform.select({
    android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    ios: PERMISSIONS.IOS.LOCATION_ALWAYS,
  }),
);
```

## Usage

It's recommended that you put your API key in a [`.env` file](https://github.com/zetachang/react-native-dotenv).

```javascript
import { Euclid } from 'react-native-ne-euclid';
import { NESDK_KEY } from 'react-native-dotenv';

export default class App extends Component<{}> {
  componentDidMount() {
  
    // Start inferring mobex audiences from sensor data
    Euclid.startRecording(NESDK_KEY);
    
    // Retrieve detected audience memberships when ready to use
    // This should be refreshed periodically
    const [memberships, setMemberships] = useState(Euclid.currentMemberships());
    
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Cohorts: {JSON.stringify(memberships, null, 2)}</Text>
      </View>
    );
  }
}
```

### Maintainers

* [Chris Watts](chris@numbereight.ai)
