npx create-react-native-library@0.34.3 react-native-ne-euclid \
    --slug "react-native-ne-euclid" \
    --description "NumberEight Euclid for React Native. This project is experimental and may be subject to change." \
    --author-name "NumberEight Technologies Ltd" \
    --author-email "support@numbereight.ai" \
    --author-url "https://numbereight.ai" \
    --repo-url "https://gitlab.com/numbereight/community/react-native-ne-euclid/" \
    --languages kotlin-objc \
    --type module-new
