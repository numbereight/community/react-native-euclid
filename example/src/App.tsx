import * as React from 'react';

import { StyleSheet, View, Text } from 'react-native';
import { Euclid } from 'react-native-ne-euclid';
import { NESDK_KEY } from '@env';

if (!NESDK_KEY) {
  throw new Error('Missing NESDK_KEY environment variable in .env file');
}

Euclid.startRecording(NESDK_KEY);

export default function App() {
  const [memberships, setMemberships] = React.useState(
    Euclid.currentMemberships()
  );

  React.useEffect(() => {
    const interval = setInterval(
      () => setMemberships(Euclid.currentMemberships()),
      5000
    );
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <View style={styles.container}>
      <Text>Cohorts: {JSON.stringify(memberships, null, 2)}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
