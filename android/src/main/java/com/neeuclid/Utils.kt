package com.neeuclid

import android.os.Build
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

@Throws(JSONException::class, IllegalArgumentException::class)
fun jsonToBundle(json: JSONObject): Bundle {
    val bundle = Bundle()
    val iterator = json.keys()
    while (iterator.hasNext()) {
        val key = iterator.next() as String
        val value = json[key]
        when (value.javaClass.simpleName) {
            "String" -> bundle.putString(key, value as String)
            "Integer" -> bundle.putInt(key, (value as Int))
            "Long" -> bundle.putLong(key, (value as Long))
            "Boolean" -> bundle.putBoolean(key, (value as Boolean))
            "JSONObject" -> bundle.putBundle(key, jsonToBundle(value as JSONObject))
            "Float" -> bundle.putFloat(key, (value as Float))
            "Double" -> bundle.putDouble(key, (value as Double))
            "JSONArray" -> bundle.putParcelableArray(key, jsonArrayToParcelableArray(value as JSONArray))
            else -> throw IllegalArgumentException(
                "${value.javaClass.simpleName} cannot be expressed in a format suitable for a Bundle"
            )
        }
    }
    return bundle
}

@Throws(JSONException::class, IllegalArgumentException::class)
fun jsonArrayToParcelableArray(json: JSONArray): Array<Parcelable> {
    val array = mutableListOf<Parcelable>()
    for (i in 0 until json.length()) {
        val value = json.get(i)
        when (value.javaClass.simpleName) {
          // Only naturally parcelable types are supported it seems
          "JSONObject" -> array.add(jsonToBundle(value as JSONObject))
//            "String" -> array.add(object : Parcelable {
//                override fun describeContents() = 0
//                override fun writeToParcel(p: Parcel, flags: Int) {
//                    p.writeString(value as String)
//                }
//            })
//            "Integer" -> array.add(object : Parcelable {
//                override fun describeContents() = 0
//                override fun writeToParcel(p: Parcel, flags: Int) {
//                    p.writeInt(value as Int)
//                }
//            })
//            "Long" -> array.add(object : Parcelable {
//                override fun describeContents() = 0
//                override fun writeToParcel(p: Parcel, flags: Int) {
//                    p.writeLong(value as Long)
//                }
//            })
//            "Boolean" -> array.add(object : Parcelable {
//                override fun describeContents() = 0
//                override fun writeToParcel(p: Parcel, flags: Int) {
//                    if (Build.VERSION.SDK_INT >= 29) {
//                        p.writeBoolean(value as Boolean)
//                    } else {
//                        p.writeByte(if (value as Boolean) 1 else 0)
//                    }
//                }
//            })
//            "Float" -> array.add(object : Parcelable {
//                override fun describeContents() = 0
//                override fun writeToParcel(p: Parcel, flags: Int) {
//                    p.writeFloat(value as Float)
//                }
//            })
//            "Double" -> array.add(object : Parcelable {
//                override fun describeContents() = 0
//                override fun writeToParcel(p: Parcel, flags: Int) {
//                    p.writeDouble(value as Double)
//                }
//            })
//            "JSONArray" -> array.add(object : Parcelable {
//                override fun describeContents() = 0
//                override fun writeToParcel(p: Parcel, flags: Int) {
//                    p.writeParcelableArray(jsonArrayToParcelableArray(value as JSONArray), flags)
//                }
//            })
            else -> {
              // Ignore non-parcelable types for now
              Log.v("NeEuclid", "Ignoring non-parcelable type ${value.javaClass.simpleName}")
//              throw IllegalArgumentException(
//                "${value.javaClass.simpleName} cannot be converted to a Parcelable value"
//              )
            }
        }
    }

    return array.toTypedArray()
}
