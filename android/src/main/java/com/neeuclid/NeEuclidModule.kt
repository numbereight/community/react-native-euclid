package com.neeuclid
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.module.annotations.ReactModule

import ai.numbereight.audiences.Audiences
import ai.numbereight.sdk.ConsentOptions
import ai.numbereight.sdk.NumberEight
import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.WritableArray

@ReactModule(name = NeEuclidModule.NAME)
class NeEuclidModule(private val reactContext: ReactApplicationContext) : NativeNeEuclidSpec(reactContext) {
    override fun getName(): String {
        return NAME
    }

    override fun startRecording(apiKey: String) {
        val token = NumberEight.start(apiKey, reactContext, ConsentOptions.useConsentManager())
        Audiences.startRecording(token)
    }

    override fun stopRecording() {
        Audiences.stopRecording()
    }

    override fun currentMemberships(): WritableArray {
        val membershipBundles = Audiences.currentMemberships.toList().map {
            jsonToBundle(it.serializeToObject())
        }
        return Arguments.makeNativeArray(membershipBundles)
    }

    companion object {
        const val NAME = "NeEuclid"
    }
}

